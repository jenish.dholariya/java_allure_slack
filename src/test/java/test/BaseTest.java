package test;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import util.driver.DriverFactory;

import static org.testng.Assert.assertEquals;
import static util.PropertyFileReader.getProperty;
import static util.driver.DriverHolder.getDriver;
import static util.driver.DriverHolder.setDriver;

public class BaseTest {

    @BeforeMethod
    public void before() {
        assertEquals(1,1);
    }

    @AfterMethod
    public void after() {
        assertEquals(1,1);
    }
}
